package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import entity.TestEntity;
import repository.TestRepository;

@Service
@Transactional
public class TestService {

	@Autowired
	private TestRepository testRepository;

	public List<TestEntity>findAllTestData(){
		return testRepository.findAll();
	}
}
