package com.testBatch.batchTest;

import java.util.List;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import entity.TestEntity;
import service.TestService;


@Configuration
@EnableBatchProcessing
public class SampleBatchApplication{
	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	@Autowired
	private TestService testService;

	private final String SAMPLE_JOB = "sampleJob";

	private final String SAMPLE_STEP = "sample_step";



	@Bean
	public Job SampleJob() {
		return this.jobBuilderFactory.get(SAMPLE_JOB)
				.start(sampleStep1())
				.build();
	}

	@Bean
	public Step sampleStep1() {
		return this.stepBuilderFactory.get(SAMPLE_STEP)
				.tasklet(sampleTasklet1())
				.tasklet(sampleTasklet2())
				.tasklet(conetcDBTestTasklet())
				.build();
	}

	private Tasklet conetcDBTestTasklet() {
		List<TestEntity> list = testService.findAllTestData();

		list.stream().forEach(i -> System.out.println(i.getText()));

		return null;
	}

	private Tasklet sampleTasklet2() {
		System.out.println("TASK1 START");
		return null;
	}

	private Tasklet sampleTasklet1() {
		System.out.println("TASK2 START");
		return null;
	}

}